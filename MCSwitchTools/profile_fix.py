#!/usr/bin/python3
# Written by Luke Chambers
# Licensed under GPLv3

from pathlib import Path
import json

path = Path(Path.home(), '.minecraft', 'launcher_profiles.json')
profiles_json = json.loads(path.read_text())
profiles = profiles_json['profiles']

for key, value in profiles.items():
	if 'javaArgs' not in value:
		value['javaArgs'] = '-Xmx1G -XX:-UseAdaptiveSizePolicy -Xmn128M'
	else:
		value['javaArgs'] = value['javaArgs'].replace('-XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode ', '')
	profiles[key] = value
	print(f'Done: {key}')

profiles_json['profiles'] = profiles
path.write_text(json.dumps(profiles_json, indent=2))
